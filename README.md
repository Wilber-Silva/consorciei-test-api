# README #

Teste de criação de api para rodar o projeto local use

* npm install
* npm start ou algum script de invoke

## Invoke Local ##

Para testar não é necessário postman use os scripts para teste local

```js
    npm run invoke:local Test // Test é o nome da função a ser invocada
    npm run invoke:local:mock requests-mock/test.json // requests-mock/test.json é o json que seria passado na requisição http
```