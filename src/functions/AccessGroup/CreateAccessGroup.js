const AWS = require('aws-sdk')
const BaseFunction = require('./../BaseFunction')

class CreateAccessGroup extends BaseFunction{
    constructor () {
        super()
        this.dynamoDb = new AWS.DynamoDB.DocumentClient()
    }
    schema = () => 
        this.joi.object({
            name: this.joi.string().max(100).min(2).required()
        })
    async create(item) {
        return this.dynamoDb.put(item).promise()
    }
    async handler ({ body }) {
        try {
            const data = await this.validate(body)
            const createObj = await this.dynamoPrepareCreateData(process.env.DYNAMO_TABLE_ACCESS_GROUP, data)

            await this.create(createObj)

            return this.responseOk(createObj.Item)
        } catch (error) {
            console.log(error)
            return this.responseError(error)
        }
    }
}
const factory = new CreateAccessGroup()

module.exports.handler = factory.handler.bind(factory)