const uuid = require('uuid')
const Joi = require('@hapi/joi')
const { ResponseBadRequestException, ResponseError, ResponseOK } = require('will-core-lib/http')

class BaseFunction {
    constructor () {
        this.joi = Joi
    }
    async validate (data) {
        if (typeof data === 'string') data = JSON.parse(data)
        const { error, value } = await this.schema().validate(
            data, { abortEarly: true }
        )
        if (error) throw new ResponseBadRequestException(error.message)
        return value
    }
    dynamoPrepareCreateData = (TableName, data) => ({
        TableName,
        Item: {
            ...data,
            id: uuid.v1(),
            createdAt: new Date().toISOString()
        }
    })
    
    responseOk = (data) => new ResponseOK(data)
    responseError = (error) => new ResponseError(error)
}

module.exports = BaseFunction