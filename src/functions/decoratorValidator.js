const { ResponseUnauthorizedException, ResponseError } = require('will-core-lib/http')

const enumParams = {
    ARG_TYPE: {
        BODY: 'body'
    }
}


const decoratorValidator = (fn, schema, argsType = enumParams) => {
    return async (event) => {
         try {
             console.log(event, argsType, event[argsType])
            const data = JSON.parse(event[argsType])
            // abortEarly == mostrar todos os erros de uma vez
            const { error, value } = await schema.validate(
                data, { abortEarly: true }
            )
           // isso faz alterar a instancia de arguments
           event[argsType] = value
           // arguments serve para pegar todos os argumentos que vieram na funcao
           // e mandar para frente
           // o apply vai retornar a função que será executada posteriormente
           console.log(error, value)
           if(!error) return fn.apply(this, arguments)
   
           throw new ResponseUnauthorizedException(error.message)
         }catch (error) {
            return new ResponseError(error)
         }
    }
}
module.exports = decoratorValidator