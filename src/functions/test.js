const { ResponseOK } = require('will-core-lib/http')

module.exports.handler = async (event) => new ResponseOK(event)